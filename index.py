from flask import Flask, render_template, url_for, request, redirect, session
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
import sqlalchemy
from sqlalchemy import Table, Column, Integer, String, ForeignKey

from dbModel import db,Companies
import psycopg2
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

def connect_db():
	conn_string = "postgres://yihldvovasswtr:88e96019646eac52632b2616679d98773274ea30fc37981232d8635a957babf2@ec2-54-227-237-223.compute-1.amazonaws.com:5432/d2ig009sis92ep"
	conn = psycopg2.connect(conn_string)
	cursor = conn.cursor()
	return conn,cursor
@app.route('/')
def default_route():
	status_login = False
	if 'user_email' in session:
		status_login = True
	return render_template('index.html', status= status_login) #, name=name
@app.route('/submit', methods=['POST'])
def submit_application():
	#print request.form
	try:
		name = request.form['name']
		email = request.form['email']
		project_languages = request.form['project_languages']
		project_title = request.form['project_title']
		description = request.form['description']
		con , cur = connect_db()
		query_insert = """
		INSERT into applications (name,email,project_languages,project_title,description)
		values (' %s','%s','%s','%s','%s')
		""" % (name, email, project_languages, project_title, description)
		#print query_insert
		cur.execute(query_insert)
		con.commit()
		return render_template('index.html', submitted = True)
		return "application submitted successfull"
	except Exception, e:
		return "Some error occured"
	return "submiteed"

@app.route('/applications')
def admin_panel():
	if 'user_email' in session:
		con,cur = connect_db()
		query_fetch = "select id, name, email, project_languages, project_title, description, status from applications order by id desc"
		cur.execute(query_fetch)
		data = cur.fetchall()
		return render_template('adminPage.html', data = data) #, name=name
	else :
		return redirect('admin') #, name=name

@app.route('/loqout')
def do_loqout():
	session.clear()

	return redirect('/admin') #, name=name

@app.route('/application/action/<int:action_id>/<int:application_id>')
def application_action(action_id = -1 ,application_id = -1):
	con, cur = connect_db()
	add = 0
	if action_id == 1:
		add = 1
	elif action_id == 2:
		add = 2
	query_update = "update applications set status='%d' where id='%d'" % (add , application_id)
	cur.execute(query_update)
	con.commit()
	return redirect('/applications')

@app.route('/admin', methods=['GET','POST'])
def admin_function():
	if 'user_email' in session:
		return redirect('/applications')
	if request.method == 'GET':
		return render_template('admin.html') #, name=name
	elif request.method == 'POST':
		try:
			email = request.form['email']
			password = request.form['password']
			con , cur = connect_db()
			query_login = "select * from users where email='%s' and password ='%s'" % (email, password)
			print query_login
			cur.execute(query_login)
			data = cur.fetchone()
			if data and len(data) > 0:
				session['user_email'] = data[1]
				return redirect('/applications')
			else :
				return render_template('admin.html', status=False) #, name=name
			

		except Exception, e:
			raise e
			return "excepttion occured"
		return "Login try"
if __name__ == '__main__':
    app.run(host='127.0.0.1')