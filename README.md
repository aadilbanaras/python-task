# Python basic test application
Live at https://pythontask.herokuapp.com/

Admin Credentials
 email : admin@admin.com
 password : admin123



App developed in python using framework Flask
live at heroku , using database of heroku.

Features,
goto website
	1) Any one can apply
	2) admin can login,
	3) admin can see application,
	4) admin can accet & cancel the application,


## Requirments

```sh
appdirs==1.4.3
click==6.7
-e git+http://github.com/pallets/flask.git@de555c82cefcb8bd61ab928481b595882c899958#egg=Flask
Flask-SQLAlchemy==2.2
gunicorn==19.6.0
itsdangerous==0.24
Jinja2==2.9.5
MarkupSafe==1.0
packaging==16.8
psycopg2==2.7.1
pyparsing==2.2.0
six==1.10.0
SQLAlchemy==1.1.8
Werkzeug==0.12.1

```

## Description

```sh
	create virtual environemnt, activate it &install dependencies / requirments written above
	for local testin, type command python index.py
	live link is "https://pythontask.herokuapp.com/" deployed at heroku
	For deployment,
		Create Procfile,manage.py,gettingstarted folder in the project directory


```
## Database Information
Tables 

                               Table "public.applications"
      Column       |          Type          |                         Modifiers                         
-------------------+------------------------+-----------------------------------------------------------
 id                | integer                | not null default nextval('applications_id_seq'::regclass)
 name              | character varying(200) | not null
 email             | character varying(50)  | not null
 project_languages | character varying(100) | not null
 project_title     | character varying(200) | not null
 description       | text                   | 
 status            | integer                | not null default 0



                                  Table "public.users"
  Column  |          Type          |                     Modifiers                      
----------+------------------------+----------------------------------------------------
 id       | integer                | not null default nextval('users_id_seq'::regclass)
 email    | character varying(200) | not null
 password | character varying(20)  | not null